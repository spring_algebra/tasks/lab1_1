/*
 * Algebra labs.
 */
 
package com.example.demo.domain;

import org.junit.Assert;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UT_Spring {

	@Test
	public void springTest() {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext();
		Assert.assertTrue("spring container should not be null", ctx != null);
		System.out.println("Spring was bootstrapped for environment " + ctx.getEnvironment());
		ctx.close();
	}

}
